/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.netbeans.modules.xslt.tmap.model.api.completion.handler;

import java.util.ArrayList;
import java.util.List;
import javax.swing.text.AbstractDocument;
import javax.swing.text.Document;
import org.netbeans.api.lexer.*;
import org.netbeans.api.xml.lexer.XMLTokenId;

/**
 *
 * @author lativ
 */
public class TMapLexerParseHelper {
    public static final String START_TAG_PREFIX = "<";
    public static final String START_TAG_SUFFIX = ">";
    public static final String END_TAG_PREFIX = "</";
    public static final String END_TAG_SUFFIX = "/>";
    public static final String DOUBLE_QUOTE = "\"";
    public static final String SINGLE_QUOTE = "'";
    public static final String EQUAL = "=";
    public static final String WHITESPACE = " ";
    
    public static XmlTag setParentTagNames(XmlTag tag, TokenSequence ts, int caretOffset) {
        assert tag != null;
        XmlTag parentTag = null;
        XmlTag currTag = tag;

        ts.move(caretOffset);
        int i = 0;
        StringBuilder sb = new StringBuilder("");

        Token currToken = ts.token();
        //looking for parent tag
        while (ts.movePrevious()) {
            currToken = ts.token();
            if (isStartOfTag(currToken)) {
            
//                currToken != null && XMLTokenId.TAG.equals(currToken.id())) {
//                if (isEndOfTag(currToken)) {
//                    continue;
//                }
                parentTag = getTagName(ts);
                if (parentTag != null) {
                    currTag.setParent(parentTag);
                    currTag = parentTag;
                }
            }
        }
        assert currToken.text() != null && currToken.text().length() > 1;

        return tag;
    }

    public static XmlTag getParentTag(TokenHierarchy th, TokenSequence ts, int caretOffset) {
        XmlTag parentTag = null;

        ts.move(caretOffset);

        Token currToken = ts.token();
        //looking for parent tag
        while (ts.movePrevious()) {
            currToken = ts.token();
            if (isStartOfTag(currToken)) {
                break;
            }
        }
        assert currToken.text() != null && currToken.text().length() > 1;
        parentTag = getTag(ts);
        if (parentTag != null) {
            int offset = currToken.offset(th);
            parentTag.setParent(getParentTag(th, ts, offset));
        }

        return parentTag;
    }

    public static XmlTag getSurroundTag(Document document, int caretOffset) {
        return getSurroundTag(document, caretOffset, false);
    }
    
    public static XmlTag getSurroundTag(Document document, int caretOffset, boolean getParent) {
        XmlTag surroundTag = null;
        assert document != null;
        ((AbstractDocument)document).readLock();
        try {
            TokenHierarchy th = TokenHierarchy.get(document);
            TokenSequence ts = th.tokenSequence();
            
            ts.move(caretOffset);
            Token currToken = ts.token();
            if (!isStartOfTag(currToken)) {
                while (ts.movePrevious()) {
                    currToken = ts.token();
                    if (isStartOfTag(currToken)) {
                        break;
                    }
                }
            }
            int currTagOffset = ts.offset();
            surroundTag = getTag(ts);
            
            if (getParent) {
                XmlTag parentTag = getParentTag(th, ts, currTagOffset);
                if (parentTag != null) {
                    surroundTag.setParent(parentTag);
                }
            } else {
                setParentTagNames(surroundTag, ts, caretOffset);
            }

        } finally {
            ((AbstractDocument)document).readUnlock();
        }        
        
        
        return surroundTag;
    }
    
    private static XmlTag getTagName(TokenSequence ts) {
        assert ts != null;
        XmlTag xmlTag = null;
        Token currToken = ts.token();
        assert isStartOfTag(currToken);
        CharSequence text = currToken.text();
        String tagName = text.toString();
        if (text.length() > 1) {
            tagName = currToken.text().subSequence(1, text.length()).toString();
        }
        xmlTag = new XmlTag(tagName, false);
        return xmlTag;
    }

    private static XmlTag getTag(TokenSequence ts) {
        assert ts != null;
        XmlTag xmlTag = null;
        Token currToken = ts.token();
//        assert currToken != null && currToken.id().equals(XMLTokenId.TAG) && !isEndOfTag(currToken);
        if (!isStartOfTag(currToken)) {
            return null;
        }
        String tagName = currToken.text().subSequence(1, currToken.text().length()).toString();
        xmlTag = new XmlTag(tagName, false);
        
        int i =0;
        StringBuilder sb = new StringBuilder("looking for the tag: "+tagName+"______________");
        
        while (ts.moveNext()) {
            Token token = ts.token();
            if (token == null) {
                continue;
            }
            TokenId tId = token.id();
            sb.append(token.text());
            i++;
            if (XMLTokenId.TAG.equals(tId)) {
                if (isEndOfTag(token)) {
                    xmlTag.setState(true);
                    break;
                }
                if (START_TAG_SUFFIX.equals(token.text())) {
                    continue;
                }
                XmlTag childTag = getTag(ts);
                if (childTag != null) {
                    xmlTag.addChildTag(xmlTag);
                }
            } else if (XMLTokenId.ARGUMENT.equals(tId)) {
                //getAttributes
                XmlAttribute xmlAttr = getAttribute(ts);
                if (xmlAttr != null) {
                    xmlTag.addAttribute(xmlAttr);
                }
            }
        }
        return xmlTag;
    }
    
    public static String getAttributeNameBeforeCaret(Document document, int caretOffset) {
        String attrNameBeforeCaret = null;
        assert document != null;
        ((AbstractDocument)document).readLock();
        try {
            TokenHierarchy th = TokenHierarchy.get(document);
            TokenSequence ts = th.tokenSequence();
            
            ts.move(caretOffset);
            Token currToken = ts.token();
            Token attrToken = isAttrToken(currToken) ? currToken : null;
            //attr is ARGUMENT OPERATOR VALUE + WSs
            if (attrToken == null) {
                boolean hasValue = currToken != null && XMLTokenId.VALUE.equals(currToken.id());
                boolean hasOperator = currToken != null && XMLTokenId.OPERATOR.equals(currToken.id());
                if (currToken == null || hasOperator || hasValue ) {
                    while (ts.movePrevious()) {
                        currToken = ts.token();
                        if (isAttrToken(currToken)) {
                            attrToken = currToken;
                            break;
                        }
                        if (currToken == null || XMLTokenId.WS.equals(currToken.id())) 
                        {
                            continue;
                        }
                        
                        if (!hasOperator && XMLTokenId.OPERATOR.equals(currToken.id())) {
                            hasOperator = true;
                            continue;
                        }
                        break;
                    }
                    
                }
                
            }
            attrNameBeforeCaret = attrToken == null ? null : attrToken.text().toString();

        } finally {
            ((AbstractDocument)document).readUnlock();
        }        
        
        
        return attrNameBeforeCaret;    
    }
    
    private static XmlAttribute getAttribute(TokenSequence ts) {
        assert ts != null;
        // completed attribute is ARGUMENT OPERATOR VALUE, may have Quotes between EQUAL and VALUE
        XmlAttribute xmlAttr = null;
        Token currToken = ts.token();
        if (currToken ==null) {
            return null;
        }
        assert currToken != null && XMLTokenId.ARGUMENT.equals(currToken.id()) && !isEndOfTag(currToken);
        CharSequence attrName = currToken.text();
        xmlAttr = new XmlAttribute(attrName.toString());
        
        StringBuilder sb = new StringBuilder("looking for the attr: "+attrName+"______________");
        
        CharSequence attrValue = null;
        boolean readValue = false;
        boolean stopParse = false;
        while (ts.moveNext()) {
            Token token = ts.token();
            if (token == null) {
                continue;
            }
            TokenId tId = token.id();
            XMLTokenId xmlTid = tId instanceof XMLTokenId ? (XMLTokenId)tId : null;
            if (xmlTid == null) {
                continue;
            }

            
            switch (xmlTid) {
                case WS:
                    stopParse = false;
                    break;
                case OPERATOR:
                    readValue = true;
                    stopParse = false;
                    break;
                case VALUE:
                    if (readValue) {
                        attrValue = token.text();
                        if (attrValue != null) {
                            xmlAttr.setValue(trimAttrValue(attrValue));
                        }
                        stopParse = true;
                    }
                    break;
                default:
                    stopParse = true;
                    break;
            }
            if (stopParse) {
                break;
            }
        }
        sb.append(" attrValue ").append(attrValue);
        return xmlAttr;
    }    
    
    private static String trimAttrValue(CharSequence value) {
        if (value == null) {
            return null;
        }
        if (TokenUtilities.startsWith(value, DOUBLE_QUOTE) 
                || TokenUtilities.startsWith(value, SINGLE_QUOTE)) 
        {
            value = value.subSequence(1, value.length());
        }
        if (TokenUtilities.endsWith(value, DOUBLE_QUOTE) 
                || TokenUtilities.endsWith(value, SINGLE_QUOTE)) 
        {
            value = value.subSequence(0, value.length() - 1);
        }
        return value.toString();
    }
    
    private static boolean isEndOfTag(Token token) {
        boolean isEndOfTag = false;
        if (token != null && XMLTokenId.TAG.equals(token.id())) {
            CharSequence tValue = token.text();
            isEndOfTag = tValue == null ? false 
                    : (TokenUtilities.startsWith(tValue, END_TAG_PREFIX) 
                    || TokenUtilities.equals(tValue, END_TAG_SUFFIX));
        }
        return isEndOfTag;
    }

    private static boolean isStartOfTag(Token token) {
        return token != null && XMLTokenId.TAG.equals(token.id()) 
                && !isEndOfTag(token) && !START_TAG_SUFFIX.equals(token.text());
    }
    
    private static boolean isAttrToken(Token token) {
        return token != null && XMLTokenId.ARGUMENT.equals(token.id());
    }

}
